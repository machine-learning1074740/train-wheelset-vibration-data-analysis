# Introduction to the Analysis

In this data analysis assignment, we will explore and interpret raw time-domain data collected from an acceleration sensor mounted on a train wheelset. The sensor captures high-sample accelerometer data along one axe (Not specified). The data is sampled 1-3 times a day while the train is in motion, with each sample lasting for 1 second. If no data exists for a particular day, it indicates that the train was stationary during that time. <u>*It is assumed that the train maintains a constant speed during the measurement.*</u>

The dataset is stored in a CSV file named *data.csv*, which includes **timestamp information**, **GPS speed** at which the sample was captured, and **data rate (DR)** signifying the sampling rate. The **magnitude of the data** is measured in milli-G (mG).
